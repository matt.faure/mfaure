# Gîtes

http://www.sudcevennes.com/Dormir/Chambres-d-hotes

## Mont Lozère

* [Mas de la Barque](https://www.lemasdelabarque.com), Mont Lozère, recommandé par David Causse

## Mont Aigoual

* [Gîte Apradi](http://www.apradis-gites.com), situé à [Saumane](https://www.openstreetmap.org/relation/2928208#map=12/44.0568/3.8200)
* [Mass Bresson](http://www.sudcevennes.com/Dormir/Gites-d-etape-et-de-groupes/gite-d%E2%80%99etape-mas-bresson/10362), La Dourbie (a priori testé avec Lolo, Caro et Bea). 3 chambres et 1 dortoir de 14 places