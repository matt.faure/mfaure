# Postfix

## Resources

* [Postfix Queue Management](https://easyengine.io/tutorials/mail/postfix-queue/) --> really nice documantation
* [Postfix Mail Queue Management](https://linuxhint.com/postfix_mail_queue_management/)
* [Commandes utiles pour Postfix](https://blog.valouille.fr/2014/04/commandes-utiles-pour-postfix/)
* [How to remove Postfix queue messages sent to a specific domain](https://serverfault.com/questions/638152/how-to-remove-postfix-queue-messages-sent-to-a-specific-domain)