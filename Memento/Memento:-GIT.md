## Git interactive Cheatsheet

[Git interactive Cheatsheet](http://www.ndpsoftware.com/git-cheatsheet.html#loc=index;)


## Remove last commit (not pushed)

```sh
git reset --hard HEAD~1
```

Sources:

* [Git Delete Last Commit](http://nakkaya.com/2009/09/24/git-delete-last-commit/)
* [How do you undo the last commit?](http://stackoverflow.com/questions/52704/how-do-you-discard-unstaged-changes-in-git)

## Git pull = ...

### By default

Git pull =

```sh
git fetch
git merge FETCH_HEAD
```
### Or with rebase

git pull --rebase = 


```sh
git fetch
git rebase origin/<currentBranch>
```

Source: [Git pull from Git-scm.com](http://git-scm.com/docs/git-pull)

## Create tag (local + remote)

```sh
git tag -a $MYTAG -m "$MYTAG"
git push origin $MYTAG
```


## Remove tag (local + remote)

```sh
git tag --delete $MYTAG
git push --delete origin $MYTAG
```

Source: [How to delete a remote tag?](http://stackoverflow.com/questions/5480258/how-to-delete-a-remote-tag)


## untrack file from Git while keeping it on filesystem

```sh
git rm --cached myFile.txt
```

Source: [Git rm](https://git-scm.com/docs/git-rm)

## git fetch doesn't fetch all branches

```shell
git config --get remote.origin.fetch                                    # Check
git config remote.origin.fetch "+refs/heads/*:refs/remotes/origin/*"    # Change
git config --get remote.origin.fetch                                    # re-check
```

Source [StackOverflow: git fetch doesn't fetch all branches](https://stackoverflow.com/questions/11623862/git-fetch-doesnt-fetch-all-branches)

## Remove local tags that are no longer on the remote repository

For short: delete locally then fetch :)

```shell
git tag -l | xargs git tag -d
git fetch --tags
```

Source [StackOverflow: Remove local git tags that are no longer on the remote repository](https://stackoverflow.com/questions/1841341/remove-local-git-tags-that-are-no-longer-on-the-remote-repository)