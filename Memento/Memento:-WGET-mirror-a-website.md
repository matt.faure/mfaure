# With WGET

Inspired from [Downloading an Entire Web Site with wget, Linux Journal](http://www.linuxjournal.com/content/downloading-entire-web-site-wget) and completed with my own options :

```bash
$ wget \
     --recursive \
     --no-clobber \
     --page-requisites \
     --html-extension \
     --convert-links \
     --restrict-file-names=windows \
     --domains website.org \
     --no-parent \
     # now my options:
     --progress=dot \
         www.website.org/tutorials/html/
```