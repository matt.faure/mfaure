From Gitlab doc [Import your project from GitHub to GitLab](https://docs.gitlab.com/ce/workflow/importing/import_projects_from_github.html)

## Import

1. [Create a new project](https://gitlab.com/projects/new?namespace_id=1928245)
1. Select `Import from: Github`

Features included:

* Automagically import git repos
* import issues
* import pull request
* import wiki pages

All of this is done by Gitlab.

## Mirroring to Github

Create an [automatic mirror on Github](https://gitlab.com/help/workflow/repository_mirroring#pushing-to-a-remote-repository) from the canonical repos on Gitlab.
  Don't forget to create / use a Github personal access token as described in [gitlab-ee#3030](https://gitlab.com/gitlab-org/gitlab-ee/issues/3030)

1. From the imported project, go to `Settings` > `Repository` > expand `Push to a remote repository`
1. Tick the checkbox `Remote mirror repository`
1. Type in the `Git repository URL` (with your _Github personal access token_). Example ``

## GitLab Configure templates

* for issues
* for merge requests

## GITHUB Update description

* Update the project _Description_: `CAUTION this is a **read-only** mirror of`
* Update the project _Website_: `GITLAB-PROJECT-URL`(the URL of the webpage on gitlab, not the Git repos URL)

## GITHUB disable (almost) everything)

From the project page > Settings, disable everything, i.e.:

* disable everything in _Feature_
* disable everything in _Merge Button_ but `Allow merge commits` (GitHub checks it back anyway)
* disable everything in _Temporary interaction limits_

## Change remotes on workstation

1. rename former github remote
  ```
git remote -v
git remote rename origin old-github
```
1. add new git remote to point to Gitlab one
  ```
git remote add origin GITLAB-REPOS-URL
```