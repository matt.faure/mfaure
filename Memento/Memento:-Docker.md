# Memento Docker

## Docker

* [Docker commands](https://docs.docker.com/engine/reference/commandline/cli/)
* [Dockerfile reference](https://docs.docker.com/engine/reference/builder/)

## Docker Composer

* [Docker compose Overview](https://docs.docker.com/compose/reference/overview/#command-options-overview-and-help)
* [Compose file reference](https://docs.docker.com/compose/compose-file/)
* [Quickstart: Compose and WordPress](https://docs.docker.com/compose/wordpress/)
* Services [startup order](https://docs.docker.com/compose/startup-order/)