# Memento Borg Backup

## General

* [quickstart](https://borgbackup.readthedocs.io/en/stable/quickstart.html)

Do not forget to read [Important note about free space](https://borgbackup.readthedocs.io/en/stable/quickstart.html#important-note-about-free-space)

**Important**: run as root, *not* with sudo

## Create a new repos for archives

```shell
borg init --encryption=none /path/to/repo
borg config /path/to/repo additional_free_space 2G
```

## List content of an archive

Remember: run **as root** (not with sudo)

```shell
borg list /media/mfaure/tera-2t2k/SAVE-Borg-Olivine10
borg list /media/mfaure/tera-2t2k/SAVE-Borg-Olivine10::olivine10-2019-02-11T09:57:54
```

Or be more precise with a path:

```shell
borg list /media/mfaure/tera-2t2k/SAVE-Borg-Olivine10::olivine10-2019-02-11T09:57:54 "home/mfaure/Documents"
```

Note: there is *no* first slash for the specified path

## Extract content of an archive

As far as I understood, when you [borg extract](https://borgbackup.readthedocs.io/en/stable/usage/extract.html) a file from an archive, you have to be on `/` (i.e. `PWD=/`)

```shell
sudo -i
cd /
borg extract /media/mfaure/tera-2t2k/SAVE-Borg-Olivine10::olivine10-2019-02-11T09:57:54 "home/mfaure/Documents"
```

Note: there is *no* first slash for the specified path

## Delete a given archive

```shell
borg delete /media/mfaure/Bigbackup/SAVE-Borg-Olivine10::olivine10-2019-02-11T19:33:18
```

Reference: [borg delete](https://borgbackup.readthedocs.io/en/stable/usage/delete.html)