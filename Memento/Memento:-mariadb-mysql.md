## Connect to a remote database

```shell
mysql --host=remoteHost --port=3306 --user=username --password myDatabase
```

## Create a user, a database, and grant rights 

```shell script
unset HISTFILE    # Do not keep the following commands in history
DB_DATABASE='asqatasun'
DB_USER='asqatasunDatabaseUserLogin'
DB_PASSWORD='asqatasunDatabaseUserP4ssword'
DB_TOHOST='localhost'

mysql -u root -p -e "CREATE USER ${DB_USER}@${DB_TOHOST} IDENTIFIED BY \"${DB_PASSWORD}\";"
mysql -u root -p -e "CREATE DATABASE ${DB_DATABASE} CHARACTER SET utf8;"
mysql -u root -p -e "GRANT ALL PRIVILEGES ON ${DB_DATABASE}.* TO ${DB_USER}@${DB_TOHOST};"
mysql -u root -p -e "FLUSH PRIVILEGES;"
```

## Mysql user with root privileges

Version 1 (pas certain de pouvoir créer un utilisateur)

```sql
CREATE USER 'myuser'@'localhost' IDENTIFIED BY '********';
GRANT ALL ON *.* TO 'myuser'@'localhost' IDENTIFIED BY '********';
GRANT GRANT OPTION ON *.* TO 'myuser'@'localhost' IDENTIFIED BY '********';
FLUSH PRIVILEGES;
```

Version 2

```sql
CREATE USER 'myuser'@'localhost' IDENTIFIED BY '********';
CREATE USER 'myuser'@'%' IDENTIFIED BY '********';
GRANT ALL PRIVILEGES ON *.* TO 'myuser'@'localhost' WITH GRANT OPTION;
GRANT ALL PRIVILEGES ON *.* TO 'myuser'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;
```

## Export a Mysql database

```shell
mysqldump \
    --host=127.0.0.1 \
    --port=3307 \
    --user=myUser \
    --password=myP4ssword \
    --no-tablespaces \
    myDatabaseName >myExport.sql
```

* Connections to `localhost` may not work whereas those to `127.0.0.1` do.
* `--no-tablespaces` may be useful. See DBA.StakExchange ['Access denied; you need (at least one of) the PROCESS privilege(s) for this operation' when trying to dump tablespaces](https://dba.stackexchange.com/questions/271981/access-denied-you-need-at-least-one-of-the-process-privileges-for-this-ope).

## Import from a MysqlDump export

```shell
mysql --user=myUser -p myNewDatabase < mysqldumpExport.sql
```

## List stored procedures / functions

```sql
SHOW PROCEDURE STATUS;
SHOW FUNCTION STATUS;
```

## Collation and character set

### Show collation of the connection (between Mysql client and Mysql server)

```sql
SELECT @@collation_connection;
```

Reference: [How to Show the Collation of your Connection in MySQL](https://database.guide/how-to-show-the-collation-of-your-connection-in-mysql/)

### Show server collation

```sql
SHOW VARIABLES LIKE 'collation%';
```

Reference: [How to Show the Server Collation in MySQL](https://database.guide/how-to-show-the-server-collation-in-mysql/)

### List a database's default character set + default collation

For database `asqatasun`

```sql
select SCHEMA_NAME, DEFAULT_CHARACTER_SET_NAME, DEFAULT_COLLATION_NAME from information_schema.SCHEMATA where SCHEMA_NAME='asqatasun';
```

Reference: [How to Show the Collation of a Database in MySQL](https://database.guide/how-to-show-database-collation-mysql/)

### List ALL tables default character set

For database `asqatasun`

```sql
SELECT T.table_name, CCSA.character_set_name 
FROM 
    information_schema.`TABLES` T, 
    information_schema.`COLLATION_CHARACTER_SET_APPLICABILITY` CCSA 
WHERE 
    CCSA.collation_name = T.table_collation AND 
    T.table_schema = "asqatasun" AND 
    T.table_name IN (SELECT table_name FROM INFORMATION_SCHEMA.tables WHERE table_schema='asqatasun');
```

Reference: [How do I see what character set a MySQL database / table / column is?](https://stackoverflow.com/questions/1049728/how-do-i-see-what-character-set-a-mysql-database-table-column-is)

### List ALL tables default collation 

For database `asqatasun`

```sql
SELECT 
   table_name,
   table_collation   
FROM information_schema.tables
WHERE table_schema = 'asqatasun';
```

Reference: [How to Show the Collation of a Table in MySQL](https://database.guide/how-to-show-the-collation-of-a-table-in-mysql/)

### Show character set + collation of all columns in all tables of a database

For database `asqatasun`

```sql
SELECT 
    TABLE_NAME,
    column_name, 
    character_set_name, 
    collation_name 
FROM information_schema.columns 
WHERE table_schema = 'asqatasun';
```

Reference: [How to Show the Collation of a Column in MySQL](https://database.guide/how-to-show-the-collation-of-a-column-in-mysql/)

## List keys and indexes of a table

For a table `PARAMETER`:

```sql
show create table PARAMETER;
```
