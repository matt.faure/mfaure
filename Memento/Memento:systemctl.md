# Memento systemctl

```shell
systemctl list-unit-files --type service --state=enabled
systemctl stop gitlab-runner.service
systemctl disable gitlab-runner.service
```
