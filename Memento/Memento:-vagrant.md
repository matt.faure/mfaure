# Memento Vagrant

## Basic commands

* `vagrant up`: launch vagrant box
* `vagrant ssh`: connect to the vagrant box
* `vagrant halt`: gracefully shut down the guest operating system and power down the guest machine
* `vagrant destroy -f`: stops the running machine Vagrant is managing and destroys all resources that were created during the machine creation process

## References

* [Vagrant Getting Started](https://www.vagrantup.com/intro/getting-started/index.html)
* [Vagrant CLI reference](https://www.vagrantup.com/docs/cli/)