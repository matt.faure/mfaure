# Dolibarr : Déménagement Ubuntu --> Mint

## Principe

1. Répéter jusqu'à atteindre la dernière version stable
    1. Sauvegarde complète
    1. Montée de version
1. Sauvegarde complète
1. Installation sur nouvelle machine de Dolibarr dernière version stable
1. Import de la sauvegarde

## Sauvegarde complète

Étapes **en tant qu'administrateur** :

1. Sauvegarde Base de Données : Accueil > Outils d'administration > Sauvegarde 
1. Sauvegarde du fichier de conf 
    * Emplacent pour une install en .deb : `/etc/dolibarr/*`
1. Sauvegarde du dossier `Documents`.
    * Emplacent pour une install en .deb : `/var/lib/dolibarr/documents`
    * vérifier avec `grep -i dolibarr_main_data_root /etc/dolibarr/conf.php`

Ressources :

* [transfert d’un ordinateur à un autre](https://www.dolibarr.fr/forum/t/transfert-dun-ordinateur-a-un-autre/22886)
* [DOC > backups](https://wiki.dolibarr.org/index.php?title=Backups)

## Montée de version

Téléchargement Dolibarr: [SourceForge Dolibarr files](https://sourceforge.net/projects/dolibarr/files/s/dolibarr/files/) puis voir DoliDeb (package Debian)

### Étapes

```shell
sudo dpkg -i dolibarr_x.y.z-w.w_all.deb
sudo apt-get install -f
```

Puis consulter http://localhost/dolibarr/

### Gestion de l'erreurs `Row size too large`

Exemple d'erreur complète : 

```log
Erreur DB_ERROR_1118: ALTER TABLE llx_facture_fourn ADD COLUMN last_main_doc varchar(255);
Row size too large. The maximum row size for the used table type, not counting BLOBs, is 8126. This includes storage overhead, check the manual. You have to change some columns to TEXT or BLOBs
```

Solution **pour l'erreur donnée en exemple** : 

* Se connecter à MariaDB en tent qu'utilisateur `dolibarr` (cf credentials dans fichier de conf)
* Modifier la table incriminée avec `ALTER TABLE llx_facture_fourn ROW_FORMAT=DYNAMIC;`
* Puis rejouer la commande en erreur, i.e. `ALTER TABLE llx_facture_fourn ADD COLUMN last_main_doc varchar(255);`

Ressources  :

* [Dolibarr for Ubuntu or Debian](https://wiki.dolibarr.org/index.php?title=Dolibarr_for_Ubuntu_or_Debian)

## Installation

Dépendances pour Mint 20 (Ubuntu 20.04)

```shell script
sudo a2dismod php7.4
sudo a2enmod php8.0
sudo systemctl restart apache2
sudo apt install php-mysql php-curl php-gd php-gd php-zip php-pear php-mail-mime php-intl mariadb-client
sudo apt install -f
sudo systemctl restart apache2 
```

## Import de données

Ressources :

* [Wiki Dolibarr > Restores](https://wiki.dolibarr.org/index.php?title=Restores)