# NextCloud

* [DOC: upgrade](https://docs.nextcloud.com/server/13/admin_manual/maintenance/upgrade.html)
* [DOC: update using command line](https://docs.nextcloud.com/server/13/admin_manual/maintenance/update.html#using-the-command-line-based-updater)

From the `updater/` directory:

```shell
sudo -u www-data php updater.phar --no-interaction
```

## 1. Migration des données d'une instance à l'autre

Contexte : migration perso TQ vers Zaclys

### Contacts / carnet d'adresses

* Depuis Thunderbird, Cardbook, exporter le carnet d'adresse (format VCF)
* L'importer depuis le nouveau NextCloud, appli Contact

### Agendas

* Depuis Thunderbird, Lignthing, exporter l'agenda (format ICS)
* L'importer depuis le nouveau NextCloud > appli Agenda > Paramètres & importation (en bas à gauche)
    * /!\ Choisir d'importer dans un **nouvel agenda**

### Photos du téléphone

`TODO`

### Notes

Copier/coller

## 2. Configuration des périphériques pour le nouveaux cloud Zaclys

Thunderbird :

* [Synchronisation des agendas et des tâches avec Thunderbird](https://wiki.zaclys.com/index.php/Synchronisation_des_agendas_et_des_t%C3%A2ches_avec_Thunderbird)
* [Synchronisation des contacts avec Thunderbird (Cardbook)](https://wiki.zaclys.com/index.php/Synchronisation_des_contacts_avec_Thunderbird_(Cardbook))

Android :

* [Synchronisation des agendas, des tâches et des contacts sous Android (DAVx5)](https://wiki.zaclys.com/index.php/Synchronisation_des_agendas,_des_t%C3%A2ches_et_des_contacts_sous_Android_(DAVx5))
* [Synchronisation des notes sous Android (Nextcloud)](https://wiki.zaclys.com/index.php/Synchronisation_des_notes_sous_Android_(Nextcloud))
