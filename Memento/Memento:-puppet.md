# Puppet

## Do not mess...

* puppet version
* with stdlib version

Both are distinct. Example: I can use Puppet 5 with stdlib 6.

## Specifying versions in Puppetfile

* ... is quite mandatory in a production environment
* but it is ok not to specify them in the `fixtures.yml`, as this permits to see conflicts in an advanced process

## Acceptance / functional test

* https://serverspec.org/resource_types.html

## Unit test

Don't forget that unit-test in Puppet is meant to test the **catalog content**, not the actual content (that should be done in acceptance testing).

Example for ntpd:

* unit test: verify that package resource with content "ntp" and ensure present is actually in the catalog (see [rspec-puppet > test catalog](https://rspec-puppet.com/documentation/classes/#test-a-resource))
* acceptance test
    *  on Debian, verify package `ntp` is installed
    *  on CentOS, verify package `ntpd` is installed