## Connection ==================================================================

### Connect to Postgres

```sh
sudo -u postgres psql
```

### Connect as a given user

```sh
psql --password --username=myusername --host=192.168.1.1 --port=5432 mydatabase
```

(`--password` forces the password to be asked)

## User management =============================================================

### Create a super user

```sql
CREATE USER myuser WITH SUPERUSER PASSWORD 'mypassword'
```

### List users to see who is superuser (or not)

```sql
SELECT rolname, rolsuper FROM pg_roles;
```


### Create a user with all privileges on its own database

```sql
CREATE USER observatoire00 WITH PASSWORD 'observatoire00-password';
CREATE DATABASE observatoire00; 
GRANT ALL PRIVILEGES ON DATABASE observatoire00 TO observatoire00;
ALTER DATABASE observatoire00 OWNER TO observatoire00;
```

### Change the password of a given user 

```sql
ALTER ROLE username WITH PASSWORD 'NewPassword123';
```

### Lost password

http://stackoverflow.com/questions/10845998/i-forgot-the-password-i-entered-during-postgres-installation

### List roles

```postgresql
\du
```

## Psql ========================================================================

### Running SQL commands from a file

For example to import a previously exported database:

```shell
psql -U username -d database_name -f myfile.sql
```

or

```shell
psql -W -U username -f my-file.sql my-db
```

### Running SQL commands inline

From https://www.postgresql.org/docs/9.5/static/app-psql.html

```
psql <<EOF
\x
SELECT * FROM foo;
EOF
```

## Database management =========================================================

### Mysql "Describe" equivalent for Postgres

```sql
\d+ tablename
```

Source: http://stackoverflow.com/questions/109325/postgresql-describe-table

### Drop all tables keeping the database

```sql
drop schema public cascade;
create schema public;
```

Source: http://stackoverflow.com/questions/3327312/drop-all-tables-in-postgresql

### Export database

```shell
pg_dump -U username -f mydb.sql
```

### list all databases

```postgresql
\l
```

### list all tables

```postgresql
\dt
```

### list all tables in all dbs

```postgresql
\dt *.*
```

### list all functions

```postgresql
\df
```

### Change owner of a database

```postgresql
ALTER DATABASE comptoir OWNER TO comptoir;
```

### Delete a database

```postgresql
DROP DATABASE comptoir;
```

Much more info on http://www.thegeekstuff.com/2009/04/15-practical-postgresql-database-adminstration-commands/


## SQL syntax ==================================================================

### Insert syntax

[Postgres doc: INSERT](https://www.postgresql.org/docs/9.5/static/sql-insert.html)

```sql
INSERT INTO films VALUES
    ('UA502', 'Bananas', 105, '1971-07-13', 'Comedy', '82 minutes');
```

```sql
INSERT INTO films (code, title, did, date_prod, kind)
    VALUES ('T_601', 'Yojimbo', 106, '1961-06-16', 'Drama');
```

## List views

```postgresql
select * from pg_catalog.pg_views
where schemaname NOT IN ('pg_catalog', 'information_schema')
order by schemaname, viewname;
```
