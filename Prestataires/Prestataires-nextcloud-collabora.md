# Prestataires NextCloud

## Cahier des charges

* Collabora Online
* multi-utilisateurs (pour partager agenda et fichiers)
* chiffrement E2EE ? (en date du 2023-06, ne semble pas *production ready* pour Nextcloud elle-même)

## Ouvaton

<https://ouvaton.coop/la-cooperative/outils-en-partage/>

### Fonctionnalités

* Collabora Office

### Info entreprise

* Type : SA Coopérative à capital variable
* capital : sans objet
* Créée en : 2001 (!)
* Effectif :

## Nubo

<https://nubo.coop/fr/page/services/>

### Fonctionnalités

* pas encore de Collabora Office

### Info entreprise

* Type : Coopérative belge (scrl-fs, coopérative à finalité sociale)
* capital : sans objet
* Créée en : 2019
* Effectif :


## Cloudeezy

* <https://cloudeezy.com/hebergement-nextcloud/particuliers-familles.html>
* tarifs intéressants
* mais boite unipersonnelle créée en 2018 <https://www.societe.com/societe/reendex-842755936.html>

### Fonctionnalités

### Info entreprise

* Type : SASU
* capital : 5000€
* Créée en : 2018

## Mise sur Orbite

* <https://marketplace.ovhcloud.com/p/1-serveur-nextcloud>

TODO : demander si possibilité d'avoir une install chiffrée

### Fonctionnalités

### Info entreprise

* Type : SAS
* capital : 65000€
* Créée en : 2016

## Ionos 1&1

<https://www.ionos.fr/solutions-bureau/herbergement-managed-nextcloud>

### Fonctionnalités

* Collabora
* Chiffrement côté serveur
* E2EE : non

### Info entreprise

* Type : SARL, controlée par Ionos-Group (ex 1&1, groupe allemand)
* capital : 100000 €
* Créée en : 2000

## Your Own Net

<https://yourownnet.net/hebergement-nextcloud-dedie/> et
<https://yourownnet.net/packs-nextcloud/>

### Fonctionnalités

* Collabora Office

### Info entreprise

* Type : SAS
* capital : 15000€
* Créée en : 2016
* Effectif : 1-2 salariés

## Octopuce

Offre non affichée sur le site, mais clairement confirmée par Benjamin Sonntag (le boss) à MF. 

### Fonctionnalités

* Collabora Office

### Info entreprise

* Type : SARL
* capital : 50000€
* Créée en : 2005
* Effectif : 11 salariés

## Librebit

<https://www.librebit.com/en/nextcloud_en/>

## Tab

<https://cloud.tab.digital/#rec190215118>

### Fonctionnalités

* E2EE
