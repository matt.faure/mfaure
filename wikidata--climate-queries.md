https://query.wikidata.org/

## List of climates existing in Wikidata 

Query:

```
#List of climates existing in Wikidata 
#defaultView:Map

#SELECT ?climLabel ?object ?clim ?objectLabel ?coord
SELECT distinct ?climLabel ?clim
WHERE {
  ?object #wdt:P31/wdt:P279? wd:Q515;
          #wdt:P2564 wd:Q13996 ;
          #wdt:P2564 wd:Q135712 ;
          wdt:P2564 ?clim ;
          wdt:P625 ?coord .

          SERVICE wikibase:label {
               bd:serviceParam wikibase:language "en"
          }
}
```

Result

| **climLabel**	                | **clim** |
| ----------------------------- | -------------------------------------------|
| tropical climate	        | http://www.wikidata.org/entity/Q135712 |
| tropical savanna climate	| http://www.wikidata.org/entity/Q113562 |
| tropical rainforest climate	| http://www.wikidata.org/entity/Q209531 |
| tropical monsoon climate	| http://www.wikidata.org/entity/Q863882 |
| hot semi-arid climate	        | http://www.wikidata.org/entity/Q23662280 |
| humid subtropical climate	| http://www.wikidata.org/entity/Q864320 |

## All data for tropical climate (Q135712)

Query

```
#Locations of cities with Tropical Climate 
#defaultView:Table

SELECT ?object ?clim ?objectLabel ?coord
#SELECT distinct ?climLabel ?clim
WHERE {
  ?object #wdt:P31/wdt:P279? wd:Q515;
          wdt:P2564 wd:Q135712 ;
          wdt:P625 ?coord .

          SERVICE wikibase:label {
               bd:serviceParam wikibase:language "en"
          }
}
```

## Number or result by climate type


| **climLabel**	                | **clim**                                          | **# of results** |
| ----------------------------- | ------------------------------------------------- | ---------------: |
| tropical climate	        | [Q135712](http://www.wikidata.org/entity/Q135712) | 560 |
| tropical savanna climate	| [Q113562](http://www.wikidata.org/entity/Q113562) | 3 |
| tropical rainforest climate	| [Q209531](http://www.wikidata.org/entity/Q209531) | 633 |
| tropical monsoon climate	| [Q863882](http://www.wikidata.org/entity/Q863882) | 507 |
| hot semi-arid climate	        | [Q23662280](http://www.wikidata.org/entity/Q23662280) | 2 |
| humid subtropical climate	| [Q864320](http://www.wikidata.org/entity/Q864320) | 2 |

## Available climate types

```
#List all subclass of Climate
#defaultView:Table

SELECT ?object ?objectLabel
WHERE {
  ?object wdt:P279* wd:Q7937 .
           SERVICE wikibase:label {
               bd:serviceParam wikibase:language "en"
          }
}
```

Results: 80