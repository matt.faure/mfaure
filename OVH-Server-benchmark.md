To benchmark a server, use:

```
sudo pveperf
```

# Proxmox5

- OVH: EG
- cpu: Intel(R) Xeon(R) CPU W3530 @ 2.80GHz
- core: 8
- ram: 24 Go
- hard disk: SATA + raid soft

```
CPU BOGOMIPS:      44793.91
REGEX/SECOND:      1000189
HD SIZE:           9.92 GB (/dev/md1)
BUFFERED READS:    93.92 MB/sec
AVERAGE SEEK TIME: 10.54 ms
FSYNCS/SECOND:     2.53
DNS EXT:           42.57 ms
DNS INT:           35.63 ms (open-s.com)
```

# Proxmox6

- ovh: Superplan
- cpu: Intel(R) Core(TM) i5-2400 CPU @ 3.10GHz
- core: 4
- ram: 16Go
- hard disk: 2*2To SATA raid soft

```
CPU BOGOMIPS:      24741.70
REGEX/SECOND:      1301321
HD SIZE:           9.92 GB (/dev/md1)
BUFFERED READS:    124.94 MB/sec
AVERAGE SEEK TIME: 7.22 ms
FSYNCS/SECOND:     266.88
DNS EXT:           80.31 ms
DNS INT:           44.63 ms (open-s.com)
```

# Proxmox7

- ovh: MG
- cpu: Intel(R) Xeon(R) CPU E5606 @ 2.13GHz
- core: 8
- ram: 24Go
- hard disk: SATA + raid hard MegaRAID

```
CPU BOGOMIPS:      34304.92
REGEX/SECOND:      842324
HD SIZE:           9.92 GB (/dev/sda1)
BUFFERED READS:    167.08 MB/sec
AVERAGE SEEK TIME: 6.63 ms
FSYNCS/SECOND:     2806.89
DNS EXT:           49.74 ms
DNS INT:           122.26 ms (ovh.net)
```

# Proxmox8

- ovh: EG-64 2014
- CPU: Intel(R) Xeon(R) CPU E5-1650 v2 @ 3.50GHz
- RAM: 64Go
- Hard disk: Raid HARD LSI 9271-4i CacheVault 1Go 2x 3To SATA3 6Gbps

```
CPU BOGOMIPS:      83999.88
REGEX/SECOND:      1727385
HD SIZE:           19.38 GB (/dev/sda2)
BUFFERED READS:    172.54 MB/sec
AVERAGE SEEK TIME: 6.19 ms
FSYNCS/SECOND:     6072.42
DNS EXT:           19.30 ms
DNS INT:           0.89 ms (ip-5-135-142.eu)
```

# Proxmox9

- ovh: EG-32 2014
- CPU: Intel(R) Xeon(R) CPU E5-1620 v2 @ 3.70GHz
- RAM: 32Go
- Hard disk: Raid HARD LSI 9271-4i CacheVault 1Go 2x 600Go SAS + CacheCade 80Go SSD

```
CPU BOGOMIPS:      59198.24
REGEX/SECOND:      1718939
HD SIZE:           19.38 GB (/dev/sda1)
BUFFERED READS:    193.82 MB/sec
AVERAGE SEEK TIME: 3.05 ms
FSYNCS/SECOND:     6018.24
DNS EXT:           28.16 ms
DNS INT:           0.97 ms (ip-178-33-227.eu)
```

# Proxmox10

- ovh: SyS W35-1
- CPU:
- RAM: 48Go
- Hard disk: 2x2To raid soft

```
CPU BOGOMIPS:      42667.28
REGEX/SECOND:      1106260
HD SIZE:           19.38 GB (/dev/md2)
BUFFERED READS:    140.59 MB/sec
AVERAGE SEEK TIME: 7.37 ms
FSYNCS/SECOND:     544.53
DNS EXT:           36.42 ms
DNS INT:           1.46 ms (ip-188-165-253.eu)
```
# Proxmox12

- ovh: SP-64 (2015)
- CPU: [Intel Xeon E5-1620v2](http://ark.intel.com/products/75779/Intel-Xeon-Processor-E5-1620-v2-10M-Cache-3_70-GHz)
- RAM: 64 Go DDR3 ECC 1600MHz
- Hard disk: 3x 2To SATA3 6Gbps MegaRAID LSI 9271-4i Cachevault 1 Go

```
CPU BOGOMIPS:      59197.92
REGEX/SECOND:      1748828
HD SIZE:           19.38 GB (/dev/sda2)
BUFFERED READS:    259.98 MB/sec
AVERAGE SEEK TIME: 6.52 ms
FSYNCS/SECOND:     5687.96
DNS EXT:           24.84 ms
DNS INT:           3.42 ms (tanaguru.com)
```
