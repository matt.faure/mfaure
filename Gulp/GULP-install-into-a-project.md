```bash
npm install gulp --save-dev
npm install gulp-jshint \
            gulp-changed \
            gulp-imagemin \
            gulp-minify-html \
            gulp-concat \
            gulp-strip-debug \
            gulp-uglify \
            gulp-autoprefixer \
            gulp-minify-css --save-dev
mkdir -p build build_local src src/Images src/CSS src/JS build/Images build/CSS build/JS
```