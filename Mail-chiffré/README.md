# Mail chiffré

## Prestataires

* Tutanota
* ProtonMail
    * Import de clés OK ([Key management](https://protonmail.com/support/knowledge-base/pgp-key-management/))
    * Intégration avec Thunderbird : avec
* MailFence
    * Import de clés OK
* Posteo
* Hushmail (à creuser --> bof)
* [RiseUp](https://en.wikipedia.org/wiki/Riseup) ?

## Comparatif

| Fonctionnalité                              | ProtonMail              | Mailfence                  | Tutanota    | RiseUp | Posteo |
|:--------------------------------------------|:------------------------|:---------------------------|:------------|:-------|:-------|
| Accès IMAP SMTP Thunderbird                 | Possible mais technique | IMAP+POP sur compte payant | Non         |        |        |
| Import de clés                              | OK                      | OK                         | Non         |        |        |
| L'utilisateur possède la clé de chiffrement | Non                     | Oui                        |             |        |        |
| Appli mobile                                |                         | à loisir                   | Oui (libre) |        |        |
| Appli ordi                                  |                         | à loisir                   | Oui (libre) |        |        |
| Import Gmail ou autre                       | OK (*Import Assistant*) | Possible mais technique    |             |        |        |


## Ressources ProtonMail

* [Key management](https://protonmail.com/support/knowledge-base/pgp-key-management/)
* [Protonmail bridge](https://protonmail.com/bridge/), licence GPLv3 [Github Proton-bridge](https://github.com/ProtonMail/proton-bridge)
* [How to import emails with Import Assistant](https://protonmail.com/support/knowledge-base/import-assistant/)

## Ressources Mailfence

--> [MailFence Knowledge base](https://kb.mailfence.com/)

* Import
    * [How to migrate Yahoo mail to Mailfence in a few clicks!](https://blog.mailfence.com/how-to-migrate-yahoo-mail-to-mailfence/)
    * [How to migrate from Gmail to Mailfence in a few clicks!](https://blog.mailfence.com/how-to-migrate-from-gmail-to-mailfence/)
    * [How to migrate from Outlook to Mailfence in a few clicks!](https://blog.mailfence.com/how-to-migrate-from-outlook-to-mailfence/)
* Chiffrement
    * [OpenPGP encryption and digital signature](https://kb.mailfence.com/categories/openpgp-encryption-and-digital-signature/)
    * [Les meilleures pratiques de chiffrement OpenPGP](https://blog.mailfence.com/fr/meilleures-pratiques-de-cryptage-openpgp/)

## Ressources Posteo

* Chiffrement
    * [EasyGPG: How do I publish my public PGP key in Posteo's key directory via Web Key Directory (WKD)?](https://posteo.de/en/help/easygpg-how-do-i-publish-my-public-pgp-key-in-posteos-key-directory-via-web-key-directory-wkd)

## Ressources Tutanota

* [Why Tutanota does not rely on PGP](https://tutanota.com/blog/posts/innovative-encryption/#why-tutanota-does-not-rely-on-pgp)
  --> pas d'import de clés possible (i.e. non-interopérable :( )
* Reddit : ["We plan to support PGP when we add Autocrypt (already on the roadmap").](https://www.reddit.com/r/tutanota/comments/k5domt/tutanota_thunderbird_bridge/)

## Ressources généralistes

* Wikipedia [Encrypted email](https://en.wikipedia.org/wiki/Email_encryption)
* Wikipedia [Comparison of webmail providers](https://en.wikipedia.org/wiki/Comparison_of_webmail_providers)
* NextInpact [GnuPG Web Key Directory : comment diffuser simplement votre clé publique via votre serveur](https://www.nextinpact.com/article/27027/104951-gnupg-web-key-directory-comment-diffuser-simplement-votre-cle-publique-via-votre-serveur)


