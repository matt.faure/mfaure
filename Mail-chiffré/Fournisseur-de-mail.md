# Fournisseurs de mail

## Objet

Identifier un ou plusieurs fournisseurs pour héberger les mails du domaine `faure.nom.fr`.

## Cahier des charges

* Supporter les mails chiffrés. /!\ Besoin de précision :
    * Support natif de OpenPGP
    * Quid du chiffrement E2E ?
* Support des mesures antispam :
    * DKIM
    * SPF
    * (DM)ARC
    * Ressources :
        * NextInpact [Sécurité des emails : Gandi active le standard DKIM](https://www.nextinpact.com/lebrief/46389/securite-emails-gandi-active-standard-dkim)
        * NextInpact [Emails avec SPF, DKIM, DMARC, ARC et BIMI : à quoi ça sert, comment en profiter ?](https://www.nextinpact.com/article/30341/109074-emails-avec-spf-dkim-dmarc-arcet-bimi-a-quoi-ca-sert-comment-en-profiter)
        * [MXToolbox](https://mxtoolbox.com/)