## Step 1

Follow [WordPress Codex: Network install](https://codex.wordpress.org/Create_A_Network)

## Step 2

Install [WPMU Domain mapping](https://wordpress.org/plugins/wordpress-mu-domain-mapping/) + activate it for the network

## Step 3

Assuming WordPress is installed in `/var/www-vhost/wp.domain.com/`, from this directory do:

```shell
sudo cp wp-content/plugins/wordpress-mu-domain-mapping/sunrise.php wp-content/
sudo chown www-data: wp-content/sunrise.php
 ```

## Step 4

In `wp-config.php`, just after the multisite part, add the following:

```php
/* WordPress MU Domain Mapping */
define( 'SUNRISE', 'on' );
```

## Step 5

 Go to Settings > Domain mapping:

* fill in the IP address of the server and the hostname
* uncheck Remote logging