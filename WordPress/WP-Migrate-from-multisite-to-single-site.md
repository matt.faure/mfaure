# WP-Migrate-from-multisite-to-single-site

## Resources

* WP Beginner: [How to Move a Site from WordPress Multisite to Single Install](http://www.wpbeginner.com/wp-tutorials/how-to-move-a-site-from-wordpress-multisite-to-single-install/) 2014-07-22
* WP Mayor [
How to Migrate a WordPress Multisite Subsite to a Single Site](https://wpmayor.com/how-to-migrate-wordpress-multisite-subsite-to-single-site/) 2017-02-07
* SuperDevResources [Moving from WordPress multisite to single Site](https://superdevresources.com/wordpress-multisite-singlesite/) 2016-02-02

## Summary

Use the WP-CLI package [Mu-Migration](https://github.com/10up/MU-Migration)

## 1. Mu-Migration - Export

* URL: stramanari.eu
* WP install: multisite
* blog-id: 2

It is considered that `wp` is installed and in `$PATH` so that it can be called from any directory.

As a **regular user** (not root):

```
wp package install 10up/mu-migration
wp mu-migration export all stramanari.eu.zip --plugins --themes --uploads --blog_id=2
```
## 2. Setup alias for WP site

... to be able to reach the old site with another name once the DNS record has been changed for the canonical name

1. From the site's dashboard (not the Network admin dashboard) > Tools > Domain mapping : ensure at least **another** URL is configured to reach the WP site, and **make it "primary"**
1. From the Network admin dashboard > Sites: edit the site to change its "Site Address (URL)" to the alias
1. In the Apache configuration, modify the directives `ServerName` and `ServerAlias` to reflect that change. Don"t forget to reload Apache.
1. Generate a certificate for the alias with `certbot`

## 3. Revoke Let's Encrypt certificate

This is needed to ease the re-creation for the same domain but with a different IP address.

```
certbot revoke --cert-path /etc/letsencrypt/live/stramanari.eu/fullchain.pem
```

## 4. Modify DNS entry

1. Modify DNS entry to reflect new IP address
1. Test with `dig` and its argument `@my-dns-server`. Check with OVH DNS and the ISP DNS.

## 5. Create Apache Vhost

Just Do it :)

## 6. Add HTTPS

* Use Certbot to do it
* Once the certicate installed, verify website with HTTPS

## 7. Create user and db

```sql
GRANT USAGE ON * . * TO 'MyUser'@'localhost' IDENTIFIED BY 'MyPasswd' 
    WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0 ;
CREATE DATABASE IF NOT EXISTS `MyDatabase` CHARACTER SET utf8;
GRANT ALL PRIVILEGES ON `MyDatabase` . * TO 'MyUser'@'localhost';
FLUSH PRIVILEGES;
```

## 8. Create blank WP site

Data for new WP site:

* new URL: stramanari.eu
* WP install: single site

As a **regular user** (not root):

```
sudo apt install -y php-zip
wp package install 10up/mu-migration
$MY_PATH="/var/www-vhosts/stramanari.eu"
mkdir -p "$MY_PATH" 
cd "$MY_PATH"
wp core download --locale=fr_FR
wp config create --dbname=stramanari --dbuser=stramanari --prompt=dbpass # password is asked, so not stored in shell history
wp core install --url=stramanari.eu --title="Mon blog" --admin_user= --admin_email=
```

## 9. Import data

First copy the .zip file to the server, then type:

```
wp mu-migration import all /tmp/stramanari.eu.zip --new_url=stramanari.eu
```

## 10. WordPress various updates

```
wp plugin update --all
wp language core update
```

## 11. Adjust remaining data

* Add write access to Apache on directories:
    * `wp-content/uploads/` and all its content (so that users can upload attachments)
    * `.` so that WordPress be able adjust to adjust `.htaccess` and rewrite-rules
* Settings: site language
* Re-apply permalink settings as they are not applied even if the settings look good. To do so, go to **Setting > Permalink** and validate (nothing to change, just validate)
* Install plugin Broken Link Checker `wp plugin install broken-link-checker --activate` and verify broken links

## 12. Configure multiple domains

* Leverage [plugin multiple-domain](https://wordpress.org/plugins/multiple-domain)


