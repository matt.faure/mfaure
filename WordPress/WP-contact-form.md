# Forms in WordPress

[Contact Form 7](https://wordpress.org/plugins/contact-form-7/) is the master piece.

## CF7 plugins

* Plugin [ContactForm7 Accessible default](https://wordpress.org/plugins/contact-form-7-accessible-defaults/)
* Plugin [Contact Form 7 Honeypot](https://wordpress.org/plugins/contact-form-7-honeypot/)
* [Conditional Fields for Contact Form 7](https://wordpress.org/plugins/cf7-conditional-fields/)
* [Advanced Contact form 7 DB](https://wordpress.org/plugins/advanced-cf7-db/)
* [Contact Form 7 Dynamic Text Extension](https://wordpress.org/plugins/contact-form-7-dynamic-text-extension/)
* [Material Design for Contact Form 7](https://wordpress.org/plugins/material-design-for-contact-form-7/)
* [Contact Form 7 Live Preview](https://wordpress.org/plugins/cf7-live-preview/)
* [Contact Form 7 Redirection](https://wordpress.org/plugins/wpcf7-redirect/)
* [Contact Form 7 Database Addon – CFDB7](https://wordpress.org/plugins/contact-form-cfdb7/)
* [Smart Grid-Layout Design for Contact Form 7](https://wordpress.org/plugins/cf7-grid-layout/)
* [Post My CF7 Form](https://wordpress.org/plugins/post-my-contact-form-7/)
* [Contact Form 7 Multi-Step Forms](https://wordpress.org/plugins/contact-form-7-multi-step-module/)
* [Contact Form 7 Controls](https://wordpress.org/plugins/contact-form-7-extras/)

## Documentation shortcuts

* [Selectable Recipient with Pipes](https://contactform7.com/selectable-recipient-with-pipes/)
* [Date Field](https://contactform7.com/date-field/)
* [Loading JavaScript and Stylesheet Only When it is Necessary](https://contactform7.com/loading-javascript-and-stylesheet-only-when-it-is-necessary/)
* [Setting Up Mail](https://contactform7.com/setting-up-mail/)