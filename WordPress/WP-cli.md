# WP-cli

* [WP-CLI list of commands](https://developer.wordpress.org/cli/commands/)

## Update everything

```
for i in @site1 @site2 ; do 
    echo -en "\n==== $i \n" ; 
    wp "${i}" core update \
        && wp "${i}" plugin update --all \
        && wp "${i}" theme update --all \
        && wp "${i}" language core update ; 
done
```

## Create a WP site

Site name is stramanari.eu

### Create MariaDB user

```sql
GRANT USAGE ON * . * TO 'MyUser'@'localhost' IDENTIFIED BY 'MyPasswd' 
    WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0 ;
CREATE DATABASE IF NOT EXISTS `MyDatabase` CHARACTER SET utf8;
GRANT ALL PRIVILEGES ON `MyDatabase` . * TO 'MyUser'@'localhost';
FLUSH PRIVILEGES;
```

### Create actual site

As a **regular user** (not root):

```shell
cd /var/www-vhosts/stramanari.eu
wp core download --locale=fr_FR
wp config create --dbname=stramanari --dbuser=stramanari --prompt=dbpass # password is asked, so not stored in shell history
wp core install --url=t.stramanari.eu --title=my-title --admin_user=my-login --admin_password=my-passwd --admin_email=bob@sundance.com
```

## Update WordPress

```
wp core check-update
wp core update
```

For network installations, you'll also need to do

```
wp core update-db --network
```

## Update plugins 

```
wp @monAlias plugin list
wp @monAlias plugin status
wp @monAlias plugin update --all
```

## Update themes

```
wp theme list
wp theme status
wp theme update --all
```

## Update language

```
wp language core list
wp language core update
```