## Note

-> Préférer [mes notes sur WP-Migration](WP-Migrate-from-multisite-to-single-site.md)

## -----------------------------------------------------------------------------

À tester [WordPress Menu Exporter](https://wordpress.org/plugins/menu-exporter/) (2017-02: ne fonctionne pas)

Pour l'export des menus, voir aussi https://gist.github.com/hissy/6739352 mais surtout ces forks : 

* mis à jour en 2016 https://gist.github.com/raynov/5171cfb2083f35fb4e051183b47808ce
* mis à jour en 2017 https://gist.github.com/overclokk/bad83a7cb3db4263192aa52db0a60fc0

## Sur l'ancien ET le nouveau site

Installer les extensions 

* [Importateur WordPress](https://fr.wordpress.org/plugins/wordpress-importer/) (2017-02: malgré les mauvaises appréciations)
* [Widget Importer & Exporter](https://wordpress.org/plugins/widget-importer-exporter/)
* [Widget Settings Importer/Exporter](https://wordpress.org/plugins/widget-settings-importexport/)
* [Customizer Export/Import](https://wordpress.org/plugins/customizer-export-import/) (qui gère aussi les menus)

## Sur l'ancien site

* Activer les extensions 
    * Widget Importer & Exporter
    * Widget Data - Setting Import/Export Plugin
    * Customizer Export/Import
* Depuis le backoffice, aller dans Outils > Widget importer exporter, faire "Export widget".
* Depuis le backoffice, aller dans Outils > Widget settings export, cocher les widgets et exporter.
* Depuis le backoffice, aller dans Outils > Exporter, choisir "tout le contenu" et exporter.
* Depuis le site lui-même, aller dans Apparence > Personnaliser, choisir Exporter puis télécharger le fichier .dat
* Lister les plugins utilisés par le site
* Exporter le site: Outils > exporter. Choisir "Tout le contenu".


## Sur le nouveau site

### 1. Créer le nouveau site WordPress sur le nouveau WordPress

Créer le nouveau site WordPress sur le nouveau WordPress

### 2. Ajouter le thème

* téléverser le theme
* activer le thème "réseau" uniquement sur le site : 
    * Admin du réseau > Sites
    * Modifier le site, puis onglet Thèmes
    * activer le thème
* activer le thème depuis le tableau de bord du site : 
    * Aller dans Apparance > Thème
    * activer le thème

### 3. Activer les plugins

* les plugins déjà listés
* Widget Data - Setting Import/Export Plugin
* Widget Importer & Exporter
* Importateur WordPress
* Customizer Export/import

### 4. Importer les widgets

Outils > Widget importer & exporter

### 5. Importer les widgets settings

Outils > Widget Settings Import

### 6. Importer le .dat du *Customizer Export/Import*

* Apparence > Personnaliser > Export/Import > Import
* fournir le fichier .dat
* cocher la case *Download and import image files?*

### 7. Re-créer les utilisateurs

La fonction d'import export de base de WordPress, malgré ce qu'elle annonce, n'est pas capable de re-créer les utilisateurs. Aussi, il faut les créer à la main :(

### 8. Importer le site : Outils > Importer


### 9. Récupération des contenus téléversés

Rsync des contenus téléversés (les vignettes des images ne sont pas transférées p. ex.) :

```
rsync -av \
   /var/www-vhosts/$OLD_SITE/wp-content/uploads/sites/$SITE_ID/ \
   $USER@$HOST:/var/www-vhosts/$NEW_SITE/wp-content/uploads/sites/$NEW_SITE_ID/
```


## TODO ne pas oublier

* ajuster le code pour les stats web (Piwik)